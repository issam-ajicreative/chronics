window.env = {
  "ACLOCAL_PATH": "C:\\Program Files\\Git\\mingw64\\share\\aclocal;C:\\Program Files\\Git\\usr\\share\\aclocal",
  "ALLUSERSPROFILE": "C:\\ProgramData",
  "API_BASE_URL": "https://admin.editionschronics.fr/api/v1",
  "APPDATA": "C:\\Users\\HP\\AppData\\Roaming",
  "AUTH_GOOGLE_API_BASE_URL": "https://chronics.v1.ajicreative.club/api/v1/login_social/google",
  "BASE_URL": "https://admin.editionschronics.fr",
  "ChocolateyInstall": "C:\\ProgramData\\chocolatey",
  "ChocolateyLastPathUpdate": "132665791753194998",
  "CHROME_CRASHPAD_PIPE_NAME": "\\\\.\\pipe\\LOCAL\\crashpad_6396_VXKBELUJGPSKVFYH",
  "COLOR": "1",
  "COLORTERM": "truecolor",
  "COMMONPROGRAMFILES": "C:\\Program Files\\Common Files",
  "CommonProgramFiles(x86)": "C:\\Program Files (x86)\\Common Files",
  "CommonProgramW6432": "C:\\Program Files\\Common Files",
  "COMPUTERNAME": "DESKTOP-PN16AMQ",
  "COMSPEC": "C:\\Windows\\system32\\cmd.exe",
  "CONFIG_SITE": "C:/Program Files/Git/etc/config.site",
  "DISPLAY": "needs-to-be-defined",
  "DriverData": "C:\\Windows\\System32\\Drivers\\DriverData",
  "EDITOR": "notepad.exe",
  "EXEPATH": "C:\\Program Files\\Git\\bin",
  "GIT_ASKPASS": "c:\\Users\\HP\\AppData\\Local\\Programs\\Microsoft VS Code\\resources\\app\\extensions\\git\\dist\\askpass.sh",
  "GOPATH": "C:\\Users\\HP\\go",
  "HOME": "C:\\Users\\HP",
  "HOMEDRIVE": "C:",
  "HOMEPATH": "\\Users\\HP",
  "HOSTNAME": "DESKTOP-PN16AMQ",
  "INFOPATH": "C:\\Program Files\\Git\\usr\\local\\info;C:\\Program Files\\Git\\usr\\share\\info;C:\\Program Files\\Git\\usr\\info;C:\\Program Files\\Git\\share\\info",
  "INIT_CWD": "C:\\side-project\\chronics_react",
  "LANG": "en_GB.UTF-8",
  "LOCALAPPDATA": "C:\\Users\\HP\\AppData\\Local",
  "LOGONSERVER": "\\\\DESKTOP-PN16AMQ",
  "MANPATH": "C:\\Program Files\\Git\\mingw64\\local\\man;C:\\Program Files\\Git\\mingw64\\share\\man;C:\\Program Files\\Git\\usr\\local\\man;C:\\Program Files\\Git\\usr\\share\\man;C:\\Program Files\\Git\\usr\\man;C:\\Program Files\\Git\\share\\man",
  "MINGW_CHOST": "x86_64-w64-mingw32",
  "MINGW_PACKAGE_PREFIX": "mingw-w64-x86_64",
  "MINGW_PREFIX": "C:/Program Files/Git/mingw64",
  "MSYSTEM": "MINGW64",
  "MSYSTEM_CARCH": "x86_64",
  "MSYSTEM_CHOST": "x86_64-w64-mingw32",
  "MSYSTEM_PREFIX": "C:/Program Files/Git/mingw64",
  "NODE": "C:\\Program Files\\nodejs\\node.exe",
  "NODE_EXE": "C:\\Program Files\\nodejs\\\\node.exe",
  "NPM_CLI_JS": "C:\\Program Files\\nodejs\\\\node_modules\\npm\\bin\\npm-cli.js",
  "npm_command": "run-script",
  "npm_config_c:/users/hp/appdata/roaming/npm/": "",
  "npm_config_c:usershpappdataroamingnpm": "",
  "npm_config_cache": "C:\\Users\\HP\\AppData\\Roaming\\npm-cache",
  "npm_config_cache#": "C:\\Users\\HP\\AppData\\Local\\npm-cache",
  "npm_config_globalconfig": "C:\\Users\\HP\\AppData\\Roaming\\npm\\etc\\npmrc",
  "npm_config_global_prefix": "C:\\Users\\HP\\AppData\\Roaming\\npm",
  "npm_config_init_module": "C:\\Users\\HP\\.npm-init.js",
  "npm_config_local_prefix": "C:\\side-project\\chronics_react",
  "npm_config_metrics_registry": "https://registry.npmjs.org/",
  "npm_config_node_gyp": "C:\\Users\\HP\\AppData\\Roaming\\nvm\\v16.15.1\\node_modules\\npm\\node_modules\\node-gyp\\bin\\node-gyp.js",
  "npm_config_noproxy": "",
  "npm_config_prefix": "C:\\Users\\HP\\AppData\\Roaming\\npm",
  "npm_config_userconfig": "C:\\Users\\HP\\.npmrc",
  "npm_config_user_agent": "npm/8.11.0 node/v16.15.1 win32 x64 workspaces/false",
  "npm_execpath": "C:\\Users\\HP\\AppData\\Roaming\\nvm\\v16.15.1\\node_modules\\npm\\bin\\npm-cli.js",
  "npm_lifecycle_event": "start",
  "npm_lifecycle_script": "react-dotenv && react-scripts start",
  "npm_node_execpath": "C:\\Program Files\\nodejs\\node.exe",
  "npm_package_json": "C:\\side-project\\chronics_react\\package.json",
  "npm_package_name": "chronics_react",
  "npm_package_version": "0.1.0",
  "NPM_PREFIX_NPM_CLI_JS": "C:\\Users\\HP\\AppData\\Roaming\\npm\\node_modules\\npm\\bin\\npm-cli.js",
  "NUMBER_OF_PROCESSORS": "4",
  "NVM_HOME": "C:\\Users\\HP\\AppData\\Roaming\\nvm",
  "NVM_SYMLINK": "C:\\Program Files\\nodejs",
  "OLDPWD": "C:/Users/HP",
  "OneDrive": "C:\\Users\\HP\\OneDrive",
  "ORIGINAL_PATH": "C:\\Program Files\\Git\\mingw64\\bin;C:\\Program Files\\Git\\usr\\bin;C:\\Users\\HP\\bin;C:\\Users\\HP\\AppData\\Local\\Programs\\Microsoft VS Code\\bin;C:\\Users\\HP\\AppData\\Roaming\\nvm;C:\\Program Files\\nodejs;C:\\Program Files\\dotnet;C:\\Program Files\\MySQL\\MySQL Shell 8.0\\bin;C:\\Users\\HP\\AppData\\Roaming\\npm;C:\\Users\\HP\\AppData\\Roaming\\npm;C:\\Program Files\\MySQL\\MySQL Server 8.0\\bin;C:\\symfony;C:\\Users\\HP\\AppData\\Roaming\\nvm;C:\\Program Files\\nodejs",
  "ORIGINAL_TEMP": "C:/Users/HP/AppData/Local/Temp",
  "ORIGINAL_TMP": "C:/Users/HP/AppData/Local/Temp",
  "ORIGINAL_XDG_CURRENT_DESKTOP": "undefined",
  "OS": "Windows_NT",
  "PATH": "C:\\side-project\\chronics_react\\node_modules\\.bin;C:\\side-project\\node_modules\\.bin;C:\\node_modules\\.bin;C:\\Users\\HP\\AppData\\Roaming\\nvm\\v16.15.1\\node_modules\\npm\\node_modules\\@npmcli\\run-script\\lib\\node-gyp-bin;C:\\Users\\HP\\.local\\bin\\;C:\\Users\\HP\\bin;C:\\Program Files\\Git\\mingw64\\bin;C:\\Program Files\\Git\\usr\\local\\bin;C:\\Program Files\\Git\\usr\\bin;C:\\Program Files\\Git\\usr\\bin;C:\\Program Files\\Git\\mingw64\\bin;C:\\Program Files\\Git\\usr\\bin;C:\\Users\\HP\\bin;C:\\Users\\HP\\AppData\\Local\\Programs\\Microsoft VS Code\\bin;C:\\Users\\HP\\AppData\\Roaming\\nvm;C:\\Program Files\\nodejs;C:\\Program Files\\dotnet;C:\\Program Files\\MySQL\\MySQL Shell 8.0\\bin;C:\\Users\\HP\\AppData\\Roaming\\npm;C:\\Users\\HP\\AppData\\Roaming\\npm;C:\\Program Files\\MySQL\\MySQL Server 8.0\\bin;C:\\symfony;C:\\Users\\HP\\AppData\\Roaming\\nvm;C:\\Program Files\\nodejs;C:\\Program Files\\Git\\usr\\bin\\vendor_perl;C:\\Program Files\\Git\\usr\\bin\\core_perl",
  "PATHEXT": ".COM;.EXE;.BAT;.CMD;.VBS;.VBE;.JS;.JSE;.WSF;.WSH;.MSC;.PY;.PYW;.CPL",
  "PKG_CONFIG_PATH": "C:\\Program Files\\Git\\mingw64\\lib\\pkgconfig;C:\\Program Files\\Git\\mingw64\\share\\pkgconfig",
  "PLINK_PROTOCOL": "ssh",
  "PROCESSOR_ARCHITECTURE": "AMD64",
  "PROCESSOR_IDENTIFIER": "Intel64 Family 6 Model 94 Stepping 3, GenuineIntel",
  "PROCESSOR_LEVEL": "6",
  "PROCESSOR_REVISION": "5e03",
  "ProgramData": "C:\\ProgramData",
  "PROGRAMFILES": "C:\\Program Files",
  "ProgramFiles(x86)": "C:\\Program Files (x86)",
  "ProgramW6432": "C:\\Program Files",
  "PROMPT": "$P$G",
  "PSModulePath": "C:\\Users\\HP\\Documents\\WindowsPowerShell\\Modules;C:\\Program Files\\WindowsPowerShell\\Modules;C:\\Windows\\system32\\WindowsPowerShell\\v1.0\\Modules",
  "PUBLIC": "C:\\Users\\Public",
  "PWD": "C:/side-project/chronics_react",
  "SHELL": "C:\\Program Files\\Git\\usr\\bin\\bash.exe",
  "SHLVL": "2",
  "SSH_ASKPASS": "C:/Program Files/Git/mingw64/libexec/git-core/git-gui--askpass",
  "SYSTEMDRIVE": "C:",
  "SYSTEMROOT": "C:\\Windows",
  "TEMP": "C:\\Users\\HP\\AppData\\Local\\Temp",
  "TERM": "xterm-256color",
  "TERM_PROGRAM": "vscode",
  "TERM_PROGRAM_VERSION": "1.74.0",
  "TMP": "C:\\Users\\HP\\AppData\\Local\\Temp",
  "TMPDIR": "C:\\Users\\HP\\AppData\\Local\\Temp",
  "USERDOMAIN": "DESKTOP-PN16AMQ",
  "USERDOMAIN_ROAMINGPROFILE": "DESKTOP-PN16AMQ",
  "USERNAME": "HP",
  "USERPROFILE": "C:\\Users\\HP",
  "VSCODE_GIT_ASKPASS_EXTRA_ARGS": "--ms-enable-electron-run-as-node",
  "VSCODE_GIT_ASKPASS_MAIN": "c:\\Users\\HP\\AppData\\Local\\Programs\\Microsoft VS Code\\resources\\app\\extensions\\git\\dist\\askpass-main.js",
  "VSCODE_GIT_ASKPASS_NODE": "C:\\Users\\HP\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe",
  "VSCODE_GIT_IPC_HANDLE": "\\\\.\\pipe\\vscode-git-77cb0e8ab6-sock",
  "VSCODE_INJECTION": "1",
  "WINDIR": "C:\\Windows",
  "WSLENV": "WT_SESSION::WT_PROFILE_ID",
  "WT_PROFILE_ID": "{54a97dc2-0c76-47a3-a7eb-d859a3c1f2a1}",
  "WT_SESSION": "61ac4546-7bbb-47d0-9317-a52e54606435",
  "ZES_ENABLE_SYSMAN": "1",
  "_": "C:/Users/HP/AppData/Local/Programs/Microsoft VS Code/Code.exe"
};