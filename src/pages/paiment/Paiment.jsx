import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import logo from "../../images/logo.png";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import env from "react-dotenv";

const Paiment = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const stripe = useStripe();
  const elements = useElements();
  const [price, setPrice] = useState();
  const [loading, setLoading] = useState(false);
  const [loader, setLoder] = useState(true);
  const [disable, setDisable] = useState(false);
  const [error, seterror] = useState(null);

  const backHome = () => {
    navigate("/");
  };
  let bearer = "Bearer " + JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    var config = {
      method: "get",
      url: `${env.API_BASE_URL}/prices/${id}?group_fields=read_prices`,
      headers: {
        Authorization: bearer,
      },
    };

    axios(config)
      .then(function (response) {
        setPrice(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(() => {
        setLoder(false);
      });
  }, [id, bearer]);

  const handleSubmit = (ev) => {
    ev.preventDefault();

    const cardElement = elements.getElement("card");
    setLoading(true);
    try {
      if (!stripe || !elements) {
      }

      stripe.createToken(cardElement).then((payload) => {
        if (!payload || payload.error) {
          setLoading(false);
          seterror(payload.error.message);
          return;
        }
        var data = JSON.stringify({
          src_code: payload.token.id,
          default: "true",
        });

        var config = {
          method: "post",
          url: `${env.API_BASE_URL}/me/subscriptions/${id}`,
          headers: {
            Authorization: bearer,
          },
          data,
        };

        axios(config)
          .then(function (response) {
            const subscription = response.data.subscription;
            const payment_intent = response.data.payment_intent;
            if (subscription.status === "active") {
              navigate("/");
            }

            switch (payment_intent.status) {
              case "active":
                navigate("/");
                break;
              case "requires_action":
                stripe
                  .confirmCardPayment(payment_intent.client_secret)
                  .then(function (result) {
                    navigate("/");
                  });
                break;
              default:
                break;
            }
            console.log(JSON.stringify(response.data));
          })
          .catch(function () {
            seterror("Paiement invalide");
          });
      });
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div
        className="container mx-auto px-4 bg-[#F2EEE3] h-[640px]"
        id="wrapper"
      >
        <div className="inner space-y-4">
          <button
            onClick={backHome}
            type="button"
            className="inline-block mt-8 rounded-full bg-white text-black leading-normal uppercase shadow-md hover:bg-gray-700 hover:shadow-lg focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-800 focus:bg-gray-800 active:shadow-lg transition duration-150 ease-in-out w-[40px] h-[40px]"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="2.5"
              stroke="currentColor"
              className="w-9 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15.75 19.5L8.25 12l7.5-7.5"
              />
            </svg>
          </button>
        </div>
        <div className="flex items-center justify-center space-x-5 w-[329px] h-[213px]">
          <img
            src={env.BASE_URL + price && price.product.image}
            alt=""
            width="130"
            height="213"
          />
          <div>
            <h1 className="text-[29px] font-bold">Offre</h1>
            <h2 className="text-[49px] font-bold">
              {loader ? (
                <div role="status" class="max-w-sm animate-pulse">
                  <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 mb-2.5"></div>
                  <span className="sr-only">Loading...</span>
                </div>
              ) : (
                price && price.product.name
              )}
            </h2>
          </div>
        </div>
        <div className="mt-[53px] space-y-2">
          <h2 className="text-[22px] font-opensans ml-2">Récapitulatif</h2>
          <p className="text-[16px] pb-5">
            {price && price.product.title}
            <br />
            <span className="font-opensans">
              {loader ? (
                <div role="status" class="max-w-sm animate-pulse">
                  <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 mb-2.5"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
                  <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
                  <span className="sr-only">Loading...</span>
                </div>
              ) : (
                price && price.product.description
              )}
            </span>
          </p>
        </div>
        <div className="border_paiment mt-4 space-y-2">
          <div className="flex items-center justify-between">
            <span className="text-[16px] font-opensans text-[#7D7D7D]">
              Total
            </span>
            <span className="text-[18px] font-opensans text-[#7D7D7D]">
              {loader ? (
                <div role="status" className="max-w-sm animate-pulse">
                  <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-10"></div>
                  <span className="sr-only">Loading...</span>
                </div>
              ) : (
                price && price.unit_amount + "€"
              )}
            </span>
          </div>
        </div>
      </div>
      <div className="container mx-auto px-6">
        <h1 className="text-[22px] mt-5">informations de facturation</h1>
        {error ? (
          <div
            className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-4"
            role="alert"
          >
            <strong className="font-bold">erreur!</strong>
            <span className="block sm:inline">{error}</span>
            <span
              className="absolute top-0 bottom-0 right-0 px-4 py-3"
              onClick={() => seterror(null)}
            >
              <svg
                className="fill-current h-6 w-6 text-red-500"
                role="button"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <title>Close</title>
                <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
              </svg>
            </span>
          </div>
        ) : (
          ""
        )}
        <div className="mt-[35px] space-y-5">
          <div className="space-y-2">
            <span className="text-[14px] text-[#828282]">Nom de titulaire</span>
            <div className="flex flex-col mb-6">
              <div className="relative">
                <input
                  id="name"
                  type="name"
                  className="text-sm sm:text-base placeholder-gray-500 pl-6 pr-4 rounded-lg border w-full h-[58px] py-2 focus:outline-none focus:border-blue-400"
                  placeholder="Nom"
                />
              </div>
            </div>
          </div>
          <div className="space-y-2">
            <span className="text-[14px] text-[#828282]">Numéro de carte</span>
            <div className="flex flex-col mb-6">
              <div className="relative">
                <CardElement
                  onChange={(e) => {
                    setDisable(!e.empty);
                  }}
                  className="text-sm sm:text-base placeholder-gray-500 pl-6 pr-4 rounded-lg border w-full h-[58px] py-5 focus:outline-none focus:border-blue-400"
                />
              </div>
            </div>
          </div>
          <div className="space-y-10 py-[50px] grid place-items-center">
            <p className="text-[13px]">
              En sélectionnant le bouton ci-dessous, vous confirmez avoir lu et
              accepté <u className="font-opensans">les termes et conditions</u>
            </p>

            <button
              onClick={handleSubmit}
              disabled={!disable}
              type="button"
              className={`inline-block px-6 w-[318px] h-[58px] py-2.5 ${
                !disable
                  ? "bg-gray-300 rounded-full focus:outline-none focus:bg-gray-300 active:bg-gray-300 hover:bg-gray-300"
                  : "bg-[#2D4263] focus:bg-[#162030] hover:bg-[#162030] active:bg-[#1e2b41]"
              }  text-white font-medium text-[15px] leading-tight uppercase rounded-full shadow-md hover:shadow-lg focus:shadow-lg focus:outline-none focus:ring-0 active:shadow-lg transition duration-150 ease-in-out`}
            >
              {loading && stripe ? (
                <div className="flex items-center justify-center space-x-2">
                  <p className="text-white">traitement ...</p>
                  <svg
                    role="status"
                    className="inline mr-3 w-4 h-4 text-white animate-spin"
                    viewBox="0 0 100 101"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                      fill="#E5E7EB"
                    />
                    <path
                      d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                      fill="currentColor"
                    />
                  </svg>
                </div>
              ) : (
                "Payer maintenant"
              )}
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Paiment;
