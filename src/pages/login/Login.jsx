import React, { useCallback, useContext, useEffect, useState } from "react";
import group from "../../images/Group.png";
import bgHalftone from "../../images/bg-halfton.png";
import google from "../../images/google.svg";
import facebook from "../../images/facebook.svg";
import apple from "../../images/apple.svg";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../context/authContext";
import env from "react-dotenv";
import {
  LoginSocialGoogle,
  LoginSocialFacebook,
  LoginSocialApple,
} from "reactjs-social-login";

import {
  GoogleLogin,
  GoogleOAuthProvider,
  useGoogleLogin,
} from "@react-oauth/google";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

const Login = () => {
  const [user, setUser] = useState({ email: "", password: "", error: false });
  const navitage = useNavigate();
  const { dispatch } = useContext(AuthContext);
  const [provider, setProvider] = useState("");
  const [profile, setProfile] = useState(null);

  // const responseGoogle = (response) => {
  //   console.log(response);
  // };

  // const responseFacebook = (response) => {
  //   console.log(response);
  // };

  const googleLogin = useGoogleLogin({
    onSuccess: async (tokenResponse) => {
      console.log(tokenResponse);
      // fetching userinfo can be done on the client or the server
      const userInfo = await axios
        .get("https://www.googleapis.com/oauth2/v3/userinfo", {
          headers: { Authorization: `Bearer ${tokenResponse.access_token}` },
        })
        .then((res) => res.data);

      console.log(userInfo);
    },
    // flow: 'implicit', // implicit is the default
  });

  const onLoginStart = (data, provider) => {
    console.log("profile", data);
    console.log("provider", provider);
    var data_token = JSON.stringify({
      access_token: data,
    });
    var config = {
      method: "post",
      url: env.AUTH_GOOGLE_API_BASE_URL,
      // headers: {
      //   Authorization: profile,
      // },
      data: data_token,
    };

    axios(config)
      .then(function (response) {
        setUser({ ...user, error: response.data.message });
      })
      .catch(function (error) {
        setUser({ ...user, error: error.response.data.message });
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post(`${env.API_BASE_URL}/login_check`, {
        username: user.email,
        password: user.password,
      })
      .then((res) => {
        if (res.status === 200) {
          console.log("data iss", res.data);
          dispatch({ type: "LOGIN", payload: res.data.token });
          setTimeout(() => navitage("/"), 1000);
        } else {
          setUser({ ...user, error: res.data.message });
        }
      })
      .catch((err) => {
        setUser({
          ...user,
          error: err.response
            ? err.response.data.message
            : "les informations d`identification invalides",
        });
      });
  };

  return (
    <div
      className="min-h-screen flex flex-col items-center justify-center bg-[#F2EEE3]"
      id="wrapper"
    >
      <div
        className="relative w-[100%] bg-no-repeat"
        style={{
          background: `url(${bgHalftone})`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "50%",
          marginTop: "-2.5rem",
        }}
      >
        <img
          src={group}
          alt="chronics"
          width="393"
          height="385"
          className="m-auto"
        />
        <button
          type="button"
          className="absolute top-[80px] left-[20px] inline-block rounded-full bg-[#70707079] text-white leading-normal uppercase shadow-md hover:bg-gray-700 hover:shadow-lg focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-800 focus:bg-gray-800 active:shadow-lg transition duration-150 ease-in-out w-[40px] h-[40px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="2.5"
            stroke="currentColor"
            className="w-9 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5L8.25 12l7.5-7.5"
            />
          </svg>
        </button>
      </div>

      <div className="flex flex-col px-4 sm:px-6 md:px-8 lg:px-10 py-8  w-full max-w-md">
        <div className="font-opensansBold text-[37px] self-center text-xl sm:text-2xl text-gray-800">
          Se Connecter
        </div>

        {user.error ? (
          <div
            className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mt-4"
            role="alert"
          >
            <strong className="font-bold">erreur!</strong>
            <span className="block sm:inline">{user.error}</span>
            <span
              className="absolute top-0 bottom-0 right-0 px-4 py-3"
              onClick={() => setUser({ ...user, error: "" })}
            >
              <svg
                className="fill-current h-6 w-6 text-red-500"
                role="button"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <title>Close</title>
                <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
              </svg>
            </span>
          </div>
        ) : (
          ""
        )}

        <div className="mt-10">
          <form action="#">
            <div className="flex flex-col mb-6">
              <div className="relative">
                <input
                  id="email"
                  type="email"
                  onChange={(e) => {
                    setUser({ ...user, email: e.target.value });
                  }}
                  name="email"
                  className="text-sm sm:text-base placeholder-gray-500 pl-6 pr-4 rounded-lg borde w-full h-[58px] py-2 focus:outline-none focus:border-blue-400"
                  placeholder="Adresse email"
                />
              </div>
            </div>
            <div className="flex flex-col mb-6">
              <div className="relative">
                <input
                  id="password"
                  type="password"
                  onChange={(e) => {
                    setUser({ ...user, password: e.target.value });
                  }}
                  name="password"
                  className="text-sm sm:text-base placeholder-gray-500 pl-6 pr-4 rounded-lg border w-full h-[58px] py-2 focus:outline-none focus:border-blue-400"
                  placeholder="mote de passe"
                />
              </div>
            </div>
            {/* {JSON.stringify(user, null, 2)}
            <div className="flex items-center mb-6 -mt-4">
              <div className="flex ml-auto">
                <a
                  href="/#"
                  className="font-opensans inline-flex text-[14px] sm:text-sm text-[#DF1F26] hover:text-red-700"
                >
                  Mot de passe oublié ?
                </a>
              </div>
            </div> */}

            <div className="flex w-full">
              <button
                onClick={handleSubmit}
                type="submit"
                className="flex items-center justify-center focus:outline-none text-white text-sm sm:text-base bg-[#282828] hover:bg-[#111111] rounded-full py-2 w-full h-[58px] transition duration-150 ease-in"
              >
                <span className="mr-2 font-opensans uppercase">
                  SE CONNECTER
                </span>
              </button>
            </div>
          </form>
        </div>
        <div className="relative mt-10 h-px bg-[#282828]">
          <div className="absolute left-0 top-0 flex justify-center w-full -mt-2">
            <span className="font-opensansNormal italic text-[15px]  bg-[#F2EEE3] px-4 text-xs text-[#282828]">
              ou continuer avec
            </span>
          </div>
        </div>
        <div className="flex justify-between items-center margin-social">
          {/* <GoogleLogin
            clientId="1051836250916-4b3m1s9gbvdvp9mev07gc0skmln0oi08.apps.googleusercontent.com"
            render={(renderProps) => (
              <button
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                This is my custom Google button
              </button>
            )}
            buttonText="Login"
            onSuccess={(credentialResponse) => {
              console.log(credentialResponse);
            }}
            onError={() => {
              console.log("Login Failed");
            }}
            cookiePolicy={"single_host_origin"}
          /> */}
          <div onClick={() => googleLogin()}>
            <img src={google} width="43" height="43" alt="google" />
          </div>
          {/* <LoginSocialGoogle
            isOnlyGetToken={true}
            client_id={
              "68477936389-at8vn7dk6pnrsqifvq3ifg668uqjoqc3.apps.googleusercontent.com"
            }
            // onLoginStart={onLoginStart}
            onResolve={({ provider, data }) => {
              setProvider(provider);
              setProfile(data?.access_token);
              onLoginStart(data?.access_token, provider);
              console.log("provider", provider);
              console.log("provider data", data);
            }}
            onReject={(err) => {
              console.log(err);
            }}
          >
            <div>
              <img src={google} width="43" height="43" alt="google" />
            </div>
          </LoginSocialGoogle> */}
          {/* <FacebookLogin
            // appId="1135182080609075"
            autoLoad
            // callback={responseFacebook}
            render={(renderProps) => (
              <div onClick={renderProps.onClick}>
                <img src={facebook} width="43" height="43" alt="facebook" />
              </div>
            )}
             
          /> */}
          {/* <LoginSocialFacebook
            isOnlyGetToken
            appId={"1135182080609075"}
            // onLoginStart={onLoginStart}
            onResolve={({ provider, data }) => {
              setProvider(provider);
              setProfile(data);
              console.log("data facebook", data);
            }}
            onReject={(err) => {
              console.log(err);
            }}
          >
            <div>
              <img src={facebook} width="43" height="43" alt="facebook" />
            </div>
          </LoginSocialFacebook> */}

          <div>
            <img src={facebook} width="43" height="43" alt="facebook" />
          </div>
          <LoginSocialApple
            client_id={process.env.REACT_APP_APPLE_ID || ""}
            scope={"name email"}
            redirect_uri={"/"}
            onLoginStart={onLoginStart}
            onResolve={({ provider, data }) => {
              setProvider(provider);
              setProfile(data?.access_token);
            }}
            onReject={(err) => {
              console.log(err);
            }}
          >
            <div>
              <img src={apple} width="43" height="43" alt="apple" />
            </div>
          </LoginSocialApple>
        </div>
      </div>
    </div>
  );
};

export default Login;
