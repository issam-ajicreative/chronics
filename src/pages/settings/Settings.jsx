import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import vorace from "../../images/vorace.png";
import Vector from "../../images/Vector.png";
import logout from "../../images/logout.png";
import axios from "axios";
import avatar from "../../images/avatar.png";
import Modal from "./Modal";
import env from "react-dotenv";
import { NavLink } from "react-router-dom";

const Settings = () => {
  // const [file, setFile] = useState(null);
  const [info, setInfo] = useState(null);
  const [loader, setLoder] = useState(true);
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  let bearer = "Bearer " + JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    var config = {
      method: "get",
      url: `${env.API_BASE_URL}/me?group_fields=read_price_product,read_stripe`,
      headers: {
        Authorization: bearer,
      },
    };

    axios(config)
      .then(function (response) {
        setInfo(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        setLoder(false);
      });
  }, [bearer]);

  return (
    <div className="min-h-screen bg-black">
      <div className="container mx-auto px-6" id="wrapper">
        <div className="space-y-4">
          <button
            onClick={() => navigate("/")}
            type="button"
            className="inline-block mt-8 rounded-full bg-[#7070709f] text-white leading-normal uppercase shadow-md hover:bg-gray-700 hover:shadow-lg focus:shadow-lg focus:outline-none focus:ring-0 active:bg-gray-800 focus:bg-gray-800 active:shadow-lg transition duration-150 ease-in-out w-[40px] h-[40px]"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="2.5"
              stroke="currentColor"
              className="w-9 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15.75 19.5L8.25 12l7.5-7.5"
              />
            </svg>
          </button>
          <h2 className="font-opensansBold text-[36px] font-bold text-[#F2EEE3]">
            Réglages
          </h2>
        </div>
        <div>
          {loader ? (
            <div
              role="status"
              className="p-4 max-w-sm rounded shadow animate-pulse md:p-6 dark:border-gray-700"
            >
              <div className="flex items-center mt-4 space-x-3">
                <svg
                  className="w-[113px] h-[113px] text-gray-200 dark:text-gray-700"
                  aria-hidden="true"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                    clipRule="evenodd"
                  ></path>
                </svg>
                <div>
                  <div className="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-32 mb-2"></div>
                  <div className="w-48 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                </div>
              </div>
              <span className="sr-only">Loading...</span>
            </div>
          ) : (
            <div className="flex items-center space-x-8 relative overflow-hidden mt-[40px]">
              <div>
                {info && info.photo !== null ? (
                  <img
                    src={env.BASE_URL + info.photo}
                    alt=""
                    width="113"
                    height="113"
                    className="rounded-xl"
                  />
                ) : (
                  <img
                    src={avatar}
                    alt=""
                    width="113"
                    height="113"
                    className="rounded-xl"
                  />
                )}
                {/* 
                <button className="flex items-center justify-center w-[44px] h-[44px] bg-[#ffffff7c] rounded-full absolute top-[26%] left-[26%]">
                  <input
                    type="file"
                    // onChange={handleChange}
                    className="upload-img"
                  />
                </button> */}
              </div>
              <div className="text-[#F2EEE3] text-[20px] font-opensansBold pl-2">
                <span className="text-[26px]">{info?.first_name}</span>
                <br />
                {info?.last_name}
              </div>
            </div>
          )}

          <div className="mt-[58px]">
            <h2 className="text-[20px] text-[#F2EEE3] font-opensansBold">
              Mes informations
            </h2>
            {loader ? (
              <div
                role="status"
                className="max-w-sm animate-pulse mb-[35px] mt-4"
              >
                <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
                <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
                <div className="h-2 bg-gray-300 rounded-full dark:bg-gray-700 mb-2.5"></div>
                <span className="sr-only">Loading...</span>
              </div>
            ) : (
              <div className="space-y-2 mt-4 mb-[35px]">
                <p className="text-[15px] text-[#ffffff79]">
                  {info?.first_name} {info?.last_name}
                </p>
                <p className="text-[15px] text-[#ffffff79]">{info?.email}</p>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="border-t border-b border-[#f2eee373]" id="wrapper">
        <div className="container mx-auto px-6 py-[25px] space-y-4">
          <h2 className="text-[20px] text-[#F2EEE3] font-opensansBold">
            Votre abonnement
          </h2>
          <div className="mb-4">
            <span className="text-[15px] text-[#ffffff79]">
              {loader ? (
                <div role="status" className="max-w-sm animate-pulse">
                  <div className="h-2.5 bg-gray-300 rounded-full dark:bg-gray-700 w-10"></div>
                  <span className="sr-only">Loading...</span>
                </div>
              ) : (
                info?.default_subscription?.price.product.name
              )}
            </span>
          </div>
          <button
            onClick={() => navigate("/")}
            type="button"
            className="inline-block px-6 h-[44px] flex items-center pt-2 pb-2 bg-[#F2EEE3] text-black font-opensans text-[14px] leading-normal uppercase rounded-full shadow-md hover:bg-[#f2eee3a9] hover:shadow-lg focus:bg-[#f2eee3a2] focus:shadow-lg focus:outline-none focus:ring-0 active:bg-[#f2eee3ab] active:shadow-lg transition duration-150 ease-in-out flex align-center"
          >
            <img src={vorace} alt="" width="21" height="19" className="mr-4" />
            mise à niveau abonnement
          </button>
        </div>
      </div>
      <div className="container mx-auto px-6 pt-[25px]" id="wrapper">
        <h2 className="text-[20px] text-[#F2EEE3] font-opensansBold">
          À propos
        </h2>
        <div className="space-y-4 mt-[35px] w-[290px]">
          {/* <div className="flex items-center justify-between">
            <p className="text-[15px] text-[#ffffff79]">Aide</p>
            <img src={Vector} alt="" width="8" height="14" />
          </div> */}

          <a href="/privacy.html" className="flex items-center justify-between">
            <p className="text-[15px] text-[#ffffff79]">
              Politique de confidentialité
            </p>
            <img src={Vector} alt="" width="8" height="14" />
          </a>

          <a href="/terms.html" className="flex items-center justify-between">
            <p className="text-[15px] text-[#ffffff79]">Mentions légales</p>
            <img src={Vector} alt="" width="8" height="14" />
          </a>

          <a href="/cgu.html" className="flex items-center justify-between">
            <p className="text-[15px] text-[#ffffff79]">CGU</p>
            <img src={Vector} alt="" width="8" height="14" />
          </a>

          <a href="/cvg.html" className="flex items-center justify-between">
            <p className="text-[15px] text-[#ffffff79]">CGV</p>
            <img src={Vector} alt="" width="8" height="14" />
          </a>
        </div>
        <button
          type="button"
          className="flex items-center mt-[72px] pb-[49px] space-x-4 w-[147px] h-[30px]"
          onClick={openModal}
        >
          <img src={logout} width="20" height="20" alt="" />
          <span className="text-[15px] text-[#ffffff79]">Se deconnecter</span>
        </button>

        {isOpen ? (
          <Modal
            isOpen={isOpen}
            closeModal={closeModal}
            setIsOpen={setIsOpen}
          />
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default Settings;
