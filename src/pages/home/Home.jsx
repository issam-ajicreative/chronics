import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import OffreCard from "../../components/OffreCard";
import { EffectCoverflow, Pagination } from "swiper";
import env from "react-dotenv";
import logo from "../../images/logo_chronics.png";

const Home = () => {
  const [products, setProducts] = useState([]);
  const [info, setInfo] = useState("");
  const [loader, setLoder] = useState(true);

  const navigate = useNavigate();

  let bearer = "Bearer " + JSON.parse(localStorage.getItem("user"));
  const fectchProduct = () => {
    var config = {
      method: "get",
      url: `${env.API_BASE_URL}/products?group_fields=read_prices`,
      headers: {
        Authorization: bearer,
      },
    };
    try {
      axios(config).then((res) => {
        setProducts(res.data.data);
        console.log(res.data);
      });
    } catch (err) {
      console.error(err);
    }
  };

  function fetchInfo() {
    var config = {
      method: "get",
      url: `${env.API_BASE_URL}/me?group_fields=read_price_product,read_stripe`,
      headers: {
        Authorization: bearer,
      },
    };

    axios(config)
      .then(function (response) {
        setInfo(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        setLoder(false);
      });
  }

  useEffect(() => {
    console.log("photo", info.photo);
    fectchProduct();
    fetchInfo();
    var config = {
      method: "get",
      url: `${env.API_BASE_URL}/me?group_fields=read_price_product,read_stripe`,
      headers: {
        Authorization: bearer,
      },
    };

    axios(config)
      .then(function (response) {
        setInfo(response.data.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [bearer]);

  return (
    <div className="min-h-screen bg-black" id="wrapper">
      <div className="container mx-auto px-4 overflow-unset">
        {loader ? (
          <div role="status" className="animate-pulse">
            <div className="flex justify-end items-center mt-4 space-x-4">
              <div className="w-16 h-2.5 bg-gray-200 rounded-full dark:bg-gray-700"></div>
              <div className="w-16 h-2 bg-gray-200 rounded-full dark:bg-gray-700"></div>
              <svg
                className="w-10 h-10 text-gray-200 dark:text-gray-700"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </div>
            <span className="sr-only">Loading...</span>
          </div>
        ) : (
          <div
            className="flex items-center justify-end mt-4 space-x-4"
            onClick={() => navigate("/settings")}
          >
            <div className="font-medium text-white">
              <div>
                {info.first_name} {info.last_name}
              </div>
            </div>
            {info && info.photo !== null ? (
              <img
                className="w-10 h-10 rounded-full"
                src={env.BASE_URL + info.photo}
                alt=""
              />
            ) : (
              <div className="overflow-hidden relative w-10 h-10 bg-gray-100 rounded-full dark:bg-gray-600">
                <svg
                  className="absolute -left-1 w-12 h-12 text-gray-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                    clipRule="evenodd"
                  ></path>
                </svg>
              </div>
            )}
          </div>
        )}

        <div className="space-y-4">
          <a href="/">
            <img src={logo} alt="logo" width="120" className="mt-6" />
          </a>
          <h2 className="font-opensansBold text-[36px] font-bold text-[#F2EEE3]">
            Nos Offres
          </h2>
        </div>

        {loader ? (
          <div className="loder flex items-center justify-center h-[480px]">
            <div className="pulsar"></div>
          </div>
        ) : (
          <Swiper
            className="mt-10 w-[328px] h-[480px] swiper-style"
            effect={"coverflow"}
            grabCursor={true}
            centeredSlides={true}
            slidesPerView={"auto"}
            coverflowEffect={{
              rotate: 50,
              stretch: 0,
              depth: 100,
              modifier: 1,
              slideShadows: true,
            }}
            pagination={true}
            modules={[EffectCoverflow, Pagination]}
          >
            {products &&
              products.map((item, id) => (
                <SwiperSlide
                  className="bg-[#F2EEE3] rounded-3xl"
                  style={{ width: "313px", height: "480px" }}
                  key={id}
                >
                  <OffreCard
                    product={item}
                    key={id}
                    default_subscription={info?.default_subscription}
                  />
                </SwiperSlide>
              ))}
          </Swiper>
        )}
      </div>
    </div>
  );
};

export default Home;
