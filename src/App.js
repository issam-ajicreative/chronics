import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import React, { useContext } from "react";
import Login from "./pages/login/Login";
import { AuthContext } from "./context/authContext";
import Home from "./pages/home/Home";
import Paiment from "./pages/paiment/Paiment";
import Settings from "./pages/settings/Settings";
import { isMobile } from "react-device-detect";
import Error from "./Error";

function App() {
  const { currentUser } = useContext(AuthContext);
  const RequiredAuth = ({ children }) => {
    return currentUser ? children : <Navigate to={"/login"} />;
  };

  // if (isMobile) {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/">
            <Route path="login" element={<Login />} />
            <Route
              index
              element={
                <RequiredAuth>
                  <Home />
                </RequiredAuth>
              }
            />
            <Route
              path="/paiment/:id"
              element={
                <RequiredAuth>
                  <Paiment />
                </RequiredAuth>
              }
            />
            <Route
              path="/settings"
              element={
                <RequiredAuth>
                  <Settings />
                </RequiredAuth>
              }
            />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
  // }
  // return <Error />;
}

export default App;
