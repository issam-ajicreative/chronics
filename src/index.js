import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { AuthContextProvider } from "./context/authContext";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { GoogleOAuthProvider } from "@react-oauth/google";
const root = ReactDOM.createRoot(document.getElementById("root"));

const stripePromise = loadStripe(
  "pk_test_51LAGK4EiFDESMXwsd3t5j0mJlH8QeYggdV6YaY5TWIOMvJ8UaD5Y6w1IyD8JXANeK77TNARYhZVv0ARiWGFGtWFj005DWoiKdn"
);

root.render(
  <React.StrictMode>
    <AuthContextProvider>
      <Elements stripe={stripePromise}>
        <GoogleOAuthProvider clientId="68477936389-at8vn7dk6pnrsqifvq3ifg668uqjoqc3.apps.googleusercontent.com">
          <App />
        </GoogleOAuthProvider>
      </Elements>
    </AuthContextProvider>
  </React.StrictMode>
);
