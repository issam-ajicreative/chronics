import React, { useState } from "react";
import bookmark from "../images/bookmarks.svg";
import { Tab } from "@headlessui/react";
import { useNavigate } from "react-router-dom";
import env from "react-dotenv";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const OffreCard = ({ product, default_subscription }) => {
  const navigate = useNavigate();
  const [priceSelected, setPriceSelected] = useState(
    product.prices && product.prices[0] ? product.prices[0] : 0
  );
  const handleSubmit = () => {
    navigate(`/paiment/${priceSelected.id}`);
  };

  const findPriceProduct = (item, clicked_btn) => {
    console.log("item", item);
    console.log("clicked_btn", clicked_btn);
    setPriceSelected(item);
  };

  return (
    <>
      <div className="flex space-x-4 mx-5 mt-5">
        <div className="relative">
          <img
            className="w-[106px] h-[106px]"
            src={env.BASE_URL + product.image}
            alt=""
          />
          {default_subscription?.price.id === priceSelected.id ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              fill="currentColor"
              className="w-6 h-6 absolute top-0 text-gray-700"
            >
              <path
                fillRule="evenodd"
                d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
                clipRule="evenodd"
              />
            </svg>
          ) : (
            ""
          )}
        </div>
        <div className="space-y-2">
          <h2 className="font-bold text-[35px]">{product.name}</h2>
          <span className="font-bold text-[25px]">
            {priceSelected.unit_amount}
          </span>
          <e className="text-[13px]">
            <sup className="text-[18px]">ttc</sup>
          </e>{" "}
          <b>€ /mois</b>
        </div>
      </div>
      <Tab.Group>
        <Tab.List className="flex space-x-1 justify-end">
          <div className="border-solid border-1 border-[#707070] box-shadow-inset rounded-full mx-5 mb-5">
            {product.prices.map((item) => (
              <Tab
                className={({ selected }) =>
                  classNames(
                    "button-inner w-full rounded-full py-2.5 text-[14px] leading-5",
                    "text-white",
                    selected ? "bg-[#282828] shadow" : "text-black",
                    default_subscription?.price.id === item.id
                      ? "bg-[#2D4263] text-white-important"
                      : ""
                  )
                }
                onClick={() =>
                  findPriceProduct(item, item.recurring_interval_count)
                }
              >
                {item.recurring_interval_count === 1
                  ? "Mois"
                  : item.recurring_interval_count === 12
                  ? "Année"
                  : item.recurring_interval_count + " m"}
              </Tab>
            ))}
          </div>
        </Tab.List>
      </Tab.Group>
      <hr />

      <div className="mt-5 space-y-5 m-10">
        <div className="flex space-x-4">
          <img src={bookmark} alt="" className="pr-6" />
          <p className="text-[18px] text-black">{product.title}</p>
        </div>
        <p className="preview-text text-black">{product.description}</p>
      </div>
      <button
        onClick={handleSubmit}
        type="button"
        disabled={default_subscription?.price.id === priceSelected.id}
        className={`block m-auto px-6 py-2.5 ${
          default_subscription?.price.id === priceSelected.id
            ? "bg-gray-400 rounded-full focus:outline-none focus:bg-gray-300 active:bg-gray-300 hover:bg-gray-300"
            : "bg-[#2D4263] hover:bg-[#1e2c42] hover:shadow-lg focus:bg-[#2f4569] active:bg-[#294066]"
        } text-white font-medium text-xs leading-tight rounded-full shadow-md focus:shadow-lg focus:outline-none focus:ring-0 active:shadow-lg w-[178px] h-[44px] transition duration-150 ease-in-out`}
      >
        J'en profite
      </button>
    </>
  );
};

export default OffreCard;
