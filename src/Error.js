import React from "react";
import error from "./images/error.svg";

const Error = () => {
  return (
    <div className="error min-h-screen bg-black flex flex-row justify-center items-center">
      <img src={error} alt="error" width="400" height="40" />
    </div>
  );
};

export default Error;
