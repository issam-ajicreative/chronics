/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontFamily: {
      lemondeliver: ["LeMondeLivre"],
      opensans: ["OpenSans"],
      opensansBold: ["OpenSansBold"],
      opensansNormal: ["OpenSansNormal"],
    },
    extend: {},
  },
  plugins: [],
};
